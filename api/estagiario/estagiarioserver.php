<?php
header('Content-type: application/json');

/**
 * Sistema: Ólirum Development Management - ODM-PHP
 * Proprietário: Ólirum Sistemas Web
 * Analista Responsável: Murilo Dark
 * Data 14/11/2018
 * Todos os direitos protegidos pela lei de Copyright
 */
include './estagiariomodel.php';
class server_estagiario {

    private $retorno;
    private $dados = array();
    private $estagiarioModel;

    //construtor da classe
    public function __construct() {
        $this->estagiarioModel = new estagiariomodel();
        $this->Controle();
    }

    private function Controle() {
        $ACAO = "";
        if (isset($_SERVER['REQUEST_METHOD'])) {
            $ACAO = $_SERVER['REQUEST_METHOD'];
        }
        switch ($ACAO) {

            // INICIA O FORMULARIO PARA CADASTRO
            //############### INICIO CASE FORM_CADASTRO           
            case "READ":

                break;
            case "POST":
                $this->functioPost();
                break;
            case "DELETE":

                break;
            case "PUT":

                break;
            case "GET":
                $this->functionRead();
                break;
        }
    }

    private function functioPost() {

        $dados = file_get_contents("php://input");
        $json = json_decode($dados);

        $this->estagiarioModel->setnome($json->nome);
        $this->estagiarioModel->setemail($json->email);
        $this->estagiarioModel->setnascimento($json->nascimento);
        $this->estagiarioModel->settelefone($json->telefone);
        $this->estagiarioModel->inserirestagiario();
    }

    private function functionRead() { 
       
        $this->retorno = $this->estagiarioModel->listaestagiario();
    }

    public function getRetorno() {
        return $this->retorno;
    }

}
$server_estagiario = new server_estagiario();
echo json_encode($server_estagiario->getRetorno());
?>
         