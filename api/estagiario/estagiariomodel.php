<?php
/**
 * Sistema: Ólirum Development Management - ODM-PHP
 * Proprietário: Ólirum Sistemas Web
 * Analista Responsável: Murilo Dark
 * Data 14/10/2018
 */
include 'DB.php';
class estagiariomodel {

    /*
     * id do banco
     */
    private $id;
    /*
     * nome do estagiario
     */
    private $nome;
    /*
     * email sera utilizado como login
     */
    private $email;
    /*
     * data de nascimento
     */
    private $nascimento;
    private $telefone;
    private $db;

    //construtor da classe
    public function __construct($id = "", $nome = "", $email = "", $nascimento = "",$telefone="") {

        $this->setid($id);
        $this->setnome($nome);
        $this->setemail($email);
        $this->setnascimento($nascimento);
        $this->settelefone($telefone);
        $this->db = new DB();
    }

    public function inserirestagiario() {
        
        $dados = '';
        $dados .= "'" . $this->getnome() . "',";
        $dados .= "'" . $this->getemail() . "',";
        $dados .= "'" . $this->getnascimento() . "',";
        $dados .= "'" . $this->gettelefone() . "'";
        
    
        $sql = "INSERT INTO  estagiario (
                        nome,
                            email,
                            nascimento,
                            telefone
                            )
                VALUES ( " . $dados . ")";
        
        if ($this->db->query($sql)) {
            $this->setid($this->db->ultimoId());
            return true;
        } else {
            print_r($this->db->getErro());
            return false;
        }
    }

    public function carregaestagiario($id = false, $extra = false) {
        $sql = "SELECT * FROM estagiario WHERE id = '" . $id . "'";
        if ($extra) {
            $sql = "SELECT * FROM estagiario " . $extra;
        }
        echo $sql;
        $this->db->query($sql);
        if ($this->db->quantidadeRegistros() > 0) {
            while ($obj = $this->db->fetchObj()) {
                $this->setid($obj->id);
                $this->setnome($obj->nome);
                $this->setemail($obj->email);
                $this->setnascimento($obj->nascimento);
                $this->settelefone($obj->telefone);

                return true;
            }
        }
        return false;
    }

    public function getArrayEstagiario() {
        $arrayEstagiario = array(
            "id" => $this->getid(),
            "nome" => $this->getnome(),
            "email" => $this->getemail(),
            "nascimento" => $this->getnascimento(),
            "telefone" => $this->gettelefone()
        );
        return $arrayEstagiario;
    }

    public function listaestagiario($extra = "") {
        $sql = "SELECT * FROM estagiario " . $extra;
        $this->db->query($sql);
        $array=  array();
        if ($this->db->quantidadeRegistros() > 0) {
            while ($obj = $this->db->fetchObj()) {
                $item = new estagiariomodel($obj->id, $obj->nome, $obj->email, $obj->nascimento, $obj->telefone);
                array_push($array, $item->getArrayEstagiario());
            }
            return $array;
        } else {
            return false;
        }
    }

    public function getid() {
        return $this->id;
    }

    public function getnome() {
        return $this->nome;
    }

    public function getemail() {
        return $this->email;
    }

    public function getnascimento() {
        return $this->nascimento;
    }

    public function gettelefone() {
        return $this->telefone;
    }

    public function setid($id) {
        $this->id = $id;
    }

    public function setnome($nome) {
        $this->nome = $nome;
    }

    public function setemail($email) {
        $this->email = $email;
    }

    public function setnascimento($nascimento) {
        $this->nascimento = $nascimento;
    }

    public function settelefone($telefone) {
        $this->telefone = $telefone;
    }

}
?>
                