<?php
if (basename($_SERVER["PHP_SELF"]) == basename(__FILE__)) {
    header("location:index.php");
}
/**
 * Sistema: Ólirum Development Management - ODM-PHP
 * Proprietário: Ólirum Sistemas Web
 * Analista Responsável: Murilo Dark
 * Versão: 1.0
 * Data 11/11/2011
 * Todos os direitos protegidos pela lei de Copyright
 * Contato: www.olirum.com.br
 * Arquivo Gerado em: 05/10/2019 as 13:14:28
 * Servidor: localhost
 * Banco de origem do arquivo: solides
 */
class DB {

    /*
     * servidor local */
    private static $server = 'localhost';
    private static $usuario = 'root';
    private static $senha = 'xxxxxx';
    private static $banco = 'in8';
    private $conn;
    private $query;
    private static $mysqli;
    private static $sessao;
    private $erro = array();

    // construtor da classe já é iniciado uma conexão automaticamente ao ser instanciado
    public function __construct() {
        $this->conexao();
    }

    // cria uma conexão com o mysql
    private static function conexao() {
        // echo '<h1>Aguarde alguns instantes, Sistema em Manutencao.</h1>';
        // exit;
        //verifica se ainda não existe uma instância do mysqli, se nao existir cria uma nova
        if (empty(self::$mysqli)) {

            self::$mysqli = new mysqli(self::$server, self::$usuario, self::$senha, self::$banco);


            /*
             * This is the "official" OO way to do it,
             * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
             */
            if (self::$mysqli->connect_error) {
                die('Connect Error (' . self::$mysqli->connect_errno . ') '
                        . self::$mysqli->connect_error);
            }
        }
    }

    public function exitConexao() {
        self::$mysqli->close();
    }

    public function transacao() {
        self::$mysqli->autocommit(FALSE);
    }

    public function commit() {
        self::$mysqli->commit();
        //$this->exitConexao();
    }

    public function rollback() {
        self::$mysqli->rollback();
        //$this->exitConexao();
    }

    // faz uma query
    public function query($sql) {
        /* Create table doesn't return a resultset */
        $this->query = self::$mysqli->query($sql);
        if ($this->query) {
            return true;
        }
        $this->erro[] = self::$mysqli->error;
        //para detalhes sobre a classe que esta utilizando o método desmarque a linha abaixo
//        var_dump(debug_backtrace());
        echo '<br><br>';
        echo 'Erro na execução do sql gerado na Classe DB linha 105:<br>Erro: ' . $sql . '<br><br>';
        print_r($this->getErro());
        echo '<br>------------<br>';
        return false;
    }

    // retorna o fetchObject da ultima consulta
    public function fetchObj() {
        return $this->query->fetch_object();
    }

    // retorna o id do insert referido
    public function ultimoId() {
        //return mysqli_insert_id();
        return self::$mysqli->insert_id;
    }

    /**
     * mysqli_affected_rows() retorna o número de linhas afetadas pela ultima consulta INSERT, UPDATE, REPLACE ou DELETE
     */
    public function RegistrosAfetados() {
        return self::$mysqli->affected_rows;
    }

    // retorna a quantidade de registro encontrados
    public function quantidadeRegistros() {
        return mysqli_num_rows($this->query);
    }

    // mostra mensagem de erro na query
    public function getErro() {
        if (empty($this->erro)) {
            return false;
        } else {
            return $this->erro;
        }
    }

    /**
     * MaxId pega o maior id de registro, se nao exisitr registros retorna 0
     * @param string $campo - nome do campo id da tabela
     * @param string $tabela - nome da tabela
     * @return int - retorna o id se existir e 0 se nao existir
     */
    public function MaxId($campo, $tabela) {
        $sql = 'SELECT AUTO_INCREMENT as maximoid FROM information_schema.tables
WHERE table_name =  "' . $tabela . '"
LIMIT 1';
        //$sql = 'SELECT LAST_INSERT_ID(' . $campo . ') as maximoid FROM ' . $tabela . ' ';
        $this->query($sql);
        if ($this->quantidadeRegistros() > 0) {
            while ($obj = $this->fetchObj()) {
                return $obj->maximoid;
            }
        } else {
            return 0;
        }
    }

    /**
     * geraCodigo() funcao responsável por gerar códigos com base no id de registros
     * utilizado em tabelas que possuem codigos como produto, notas, orcamentos, etc
     * gera um codigo com 10 dígitos
     * em caso de edição passa o id do registro
     * @param string $campo - campo da tabela a ser verificado o maximo id
     * @param string $tabela - tabela a ser verificada o maximo id
     * @param string $inicial - letras iniciais para o codigo exp OS, PD, OR, será completado o restante com o maximo id e zeros
     * @param int $ndigitos - quantidade de dígitos do numero gerado inicialmente 10
     * @param int $id - se o id for passado será gerado com base no id ne não no maxid
     * @return string - numero gerado com base no max id da tabela exp N-0000012345
     */
    public function geraCodigo($campo, $tabela, $inicial = 'N', $ndigitos = 10, $id = '') {
        //pega o maior id e incrementa 1 ao final
        if (empty($id)) {
            $id = $this->MaxId($campo, $tabela);
        }
        //acrescenta zeros antes do numero
        if (empty($inicial)) {
            $numero = str_pad($id, $ndigitos, "0", STR_PAD_LEFT);
        } else {
            $numero = $inicial . '-' . str_pad($id, $ndigitos, "0", STR_PAD_LEFT);
        }

        //insere o numero
        return $numero;
    }

}
?>