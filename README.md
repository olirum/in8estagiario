# README #

### Projeto ###
	Esse projeto foi elaborado utilizando o framework angular 8 juntamente com o bootstrap 4 e o MYSQL 5.7.17-log,


### Passo a passao para configurar: ###
	
	1 Baixe os arquivo no servidor de sua preferência

	2 Configuarar API PHP - Instalar o script da base de dados mysql que encontra-se no diretório banco nome do arquivo bancoMysqlIn8.sql,
		será instalado uma base no servidor MYSQL com o nome in8.
		
	3 Configurar a classe responsável pela conexão com a base MYSQL criada no passo anterior,
		navegue até diretório api\estagiario e selecione o arquivo DB.php para editar as informações de conexão,
		com o arquivo aberto navegue até as linhas  para alterar a informações:
			21 - private static $server = 'localhost'; (endereço do servidor mysql)
    		22 - private static $usuario = 'root'; (usuário do servidor)
    		23 - private static $senha = 'xxxxxx'; (senha de acesso ao servidor),
			
	4 Configurar diretório base da aplicação:
		Abra o arquivo index.html do diretório raiz do projeto navegue até a linha 
		6 - <base href="/in8estagiario/"> e altere a href base do projeto para a base de sua instalação, 
		base do servidor onde foi configurado o site, no caso acima seria http://localhost/in8estagiario/.
	
	Com essas alterações o projeto está pronto para rodar e para isso acesse o http://localhost/in8estagiario/.
		

### Who do I talk to? ###

	Murilo Dark